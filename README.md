# GEOTALK
![alt text](Screenshots/name.png)
![alt text](Screenshots/logo.png)

The Geotalk app offers Geo-referenced chatrooms. 


## Screenshots

* *Login Page*

![alt text](Screenshots/ss1.png)

* *Map with 2 chatrooms in the 5 km range*

![alt text](Screenshots/ss2.png)

* *Chatroom description*

![alt text](Screenshots/ss3.png)

* *Chatroom creation*

![alt text](Screenshots/ss4.png)

* *Chatroom talk*

![alt text](Screenshots/ss6.png)


## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* If you like this project and you want to help me please contact me at r.seara at campus dot fct dot unl dot pt

