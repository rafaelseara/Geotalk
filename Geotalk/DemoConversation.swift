//
//  DemoConversation.swift
//  SwiftExample
//
//  Created by Dan Leonard on 5/11/16.
//  Copyright © 2016 MacMeDan. All rights reserved.
//

import JSQMessagesViewController

// User Enum to make it easyier to work with.
enum User: String {
    case Leonard    = "053496-4509-288"
    case Squires    = "053496-4509-289"
    case Jobs       = "707-8956784-57"
    case Cook       = "468-768355-23123"
    case Wozniak    = "309-41802-93823"
    
}

// Helper Function to get usernames for a secific User.
func getName(_ user: User) -> String{
    switch user {
    case .Squires:
        return "Miguel Marques"
    case .Cook:
        return "Rafael Seara"
    case .Wozniak:
        return "Calitos Tevez"
    case .Leonard:
        return "Dan Bilzerian"
    case .Jobs:
        return "Inês Eusébio"
    }
}
//// Create Names to display
//let DisplayNameSquires = "Jesse Squires"
//let DisplayNameLeonard = "Dan Leonard"
//let DisplayNameCook = "Tim Cook"
//let DisplayNameJobs = "Steve Jobs"
//let DisplayNameWoz = "Steve Wazniak"



// Create Unique IDs for avatars
let AvatarIDLeonard = "053496-4509-288"
let AvatarIDSquires = "053496-4509-289"
let AvatarIdCook = "468-768355-23123"
let AvatarIdJobs = "707-8956784-57"
let AvatarIdWoz = "309-41802-93823"

// Create Avatars Once for performance
//
// Create an avatar with Image


let AvatarLeonard = JSQMessagesAvatarImageFactory.avatarImage(withUserInitials: "DL", backgroundColor: UIColor.jsq_messageBubbleGreen(), textColor:  UIColor.white, font: UIFont.systemFont(ofSize: 12), diameter: 10)
    //JSQMessagesAvatarImageFactory(withUserInitials: "DL", backgroundColor: UIColor.jsq_messageBubbleGreen(), textColor: UIColor.white, font: UIFont.systemFont(ofSize: 12))

let AvatarCook = JSQMessagesAvatarImageFactory.avatarImage(withUserInitials: "TC", backgroundColor: UIColor.gray, textColor: UIColor.white, font: UIFont.systemFont(ofSize: 12),diameter: 10)

// Create avatar with Placeholder Image
let AvatarJobs = JSQMessagesAvatarImageFactory.avatarImage(with: UIImage(named:"demo_avatar_jobs")!, diameter: 20)

let AvatarWoz = JSQMessagesAvatarImageFactory.avatarImage(withUserInitials: "SW", backgroundColor: UIColor.jsq_messageBubbleGreen(), textColor: UIColor.white, font: UIFont.systemFont(ofSize: 12),diameter: 10)

let AvatarSquires = JSQMessagesAvatarImageFactory.avatarImage(withUserInitials: "JSQ", backgroundColor: UIColor.gray, textColor: UIColor.white, font: UIFont.systemFont(ofSize: 12),diameter: 10)

// Helper Method for getting an avatar for a specific User.
func getAvatar(_ id: String) -> JSQMessagesAvatarImage{
    print(id)
    //let user = User(rawValue: "707-8956784-57")!
    let user = User(rawValue: id)!

    
    switch user {
    case .Leonard:
        return AvatarLeonard!
    case .Squires:
        return AvatarSquires!
    case .Cook:
        return AvatarCook!
    case .Wozniak:
        return AvatarWoz!
    case .Jobs:
        return AvatarJobs!
    default:
        return AvatarJobs!
    }
}



// INFO: Creating Static Demo Data. This is only for the exsample project to show the framework at work.
var conversationsList = [Conversation]()

var convo = Conversation(firstName: "Steave", lastName: "Jobs", preferredName:  "Stevie", smsNumber: "(987)987-9879", id: "33", latestMessage: "Holy Guacamole, JSQ in swift", isRead: false)

var conversation = [JSQMessage]()

let message = JSQMessage(senderId: AvatarIdCook, displayName: getName(User.Cook), text: "Donec nec condimentum risus.")
let message2 = JSQMessage(senderId: AvatarIDSquires, displayName: getName(User.Squires), text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. In faucibus lacus metus, sit amet ornare ligula semper sed. Donec a lacus ac sapien dapibus fringilla at quis tellus.")
let message3 = JSQMessage(senderId: AvatarIdWoz, displayName: getName(User.Wozniak), text: "Quisque a feugiat nulla, sit amet tempor velit. Nam vestibulum eu ligula at lobortis. Ut tincidunt dapibus diam, sit amet tempus urna efficitur vel")
let message4 = JSQMessage(senderId: AvatarIdJobs, displayName: getName(User.Jobs), text: "Morbi non ullamcorper erat, eu scelerisque augue.")
let message5 = JSQMessage(senderId: AvatarIDLeonard, displayName: getName(User.Leonard), text: "bye.")


let message6 = JSQMessage(senderId: AvatarIDLeonard, displayName: getName(User.Leonard), text: "This is incredible")
let message7 = JSQMessage(senderId: AvatarIdWoz, displayName: getName(User.Wozniak), text: "I would have to agree")
let message8 = JSQMessage(senderId: AvatarIDLeonard, displayName: getName(User.Leonard), text: "It is unit-tested, free, open-source, and documented like a boss.")
let message9 = JSQMessage(senderId: AvatarIdWoz, displayName: getName(User.Wozniak), text: "You guys need an award for this, I'll talk to my people at Apple. 💯 💯 💯")

// photo message
let photoItem = JSQPhotoMediaItem(image: UIImage(named: "goldengate"))
let photoMessage = JSQMessage(senderId: AvatarIdWoz, displayName: getName(User.Wozniak), media: photoItem)

// audio mesage
let sample = Bundle.main.path(forResource: "jsq_messages_sample", ofType: "m4a")
let audioData = try? Data(contentsOf: URL(fileURLWithPath: sample!))
let audioItem = JSQAudioMediaItem(data: audioData)
let audioMessage = JSQMessage(senderId: AvatarIdWoz, displayName: getName(User.Wozniak), media: audioItem)

func makeGroupConversation()->[JSQMessage] {
    //conversation = [message!, message2!,message3!, message4!, message5!, photoMessage!, audioMessage!]
    conversation = [message!, message2!,message3!, message4!, message5!, photoMessage!, photoMessage!]

    return conversation
}

func makeNormalConversation() -> [JSQMessage] {
    conversation = [message6!, message7!, message8!, message9!, photoMessage!, audioMessage!]
    return conversation
}
