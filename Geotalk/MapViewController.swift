//
//  MapViewController.swift
//  Geotalk
//
//  Created by Rafael Seara on 25/03/17.
//  Copyright © 2017 Rafael Seara. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import FBSDKLoginKit

class MapViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    @IBOutlet weak var mapView: MKMapView!
    // MARK: - Map view delegate
    
    let ref = FIRDatabase.database().reference();

    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        var view = mapView.dequeueReusableAnnotationView(withIdentifier: "AnnotationView Id")
        if view == nil{
            view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "AnnotationView Id")
            view!.canShowCallout = true
        } else {
            view!.annotation = annotation
        }
        
        view?.leftCalloutAccessoryView = nil
        view?.rightCalloutAccessoryView = UIButton(type: UIButtonType.detailDisclosure)
        //swift 1.2
        //view?.rightCalloutAccessoryView = UIButton.buttonWithType(UIButtonType.DetailDisclosure) as UIButton
        
        return view
    }
    
    func mapView(mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        //I don't know how to convert this if condition to swift 1.2 but you can remove it since you don't have any other button in the annotation view
        if (control as? UIButton)?.buttonType == UIButtonType.detailDisclosure {
            mapView.deselectAnnotation(view.annotation, animated: false)
            //performSegueWithIdentifier("you're segue Id to detail vc", sender: view)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        showPoints()
    }

    let locationManager = CLLocationManager()
    
    var talkPoints = [mapTalkObj]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self

        // My locarion
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        
        self.locationManager.startUpdatingLocation()
        
        
        self.mapView.showsUserLocation = true
        //self.MapView.isZoomEnabled = false;
        //self.MapView.isScrollEnabled = false;

        
        getPoiData()

    }
    

    func getPoiData(){
        print("DATA")
        let userID = FIRAuth.auth()?.currentUser?.uid
        //print("UserID:\(userID)")
        ref.child("chats").observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            //print(snapshot.value!)
            let values = snapshot.value as! NSArray
            
            for item in values { // loop through data items
                let obj = item as! NSDictionary
                let id =  obj["id"] as! String
                let lat = obj["lat"] as! Double
                let lon = obj["lon"] as! Double
                let title = obj["subject"] as! String
                let desc = obj["description"] as! String
                print("added")
                self.talkPoints.append(mapTalkObj.init(id: id, lat: lat, long: lon, title: title, desc: desc))
            }
            
            self.showPoints()
            
        }) { (error) in
            print(error.localizedDescription)
        }
    }
    
    func showPoints(){
        print("Hello darkness")
        print(talkPoints.capacity)
        for point in talkPoints{
            
            let annotationView = MKAnnotationView()
            let detailButton: UIButton = UIButton.init(type: UIButtonType.detailDisclosure) as UIButton
            annotationView.rightCalloutAccessoryView = detailButton

        
            let myCoordinate = locationManager.location?.coordinate
            let location = CLLocationCoordinate2D(latitude: point.lat, longitude: point.long)
            let point1 = MKMapPointForCoordinate(myCoordinate!)
            let point2 = MKMapPointForCoordinate(location)

            let distanceInMeters = MKMetersBetweenMapPoints(point1, point2)
            
            if(distanceInMeters <= 500){
                let annotation = MKPointAnnotation()
                annotationView.annotation = annotation
                annotation.coordinate = location
                annotation.title = point.title
                annotation.subtitle = point.desc
                annotation.accessibilityValue = point.id
                //let btn = UIButton(type: .detailDisclosure)
                    //annotation.rightCalloutAccessoryView = btn
                mapView.addAnnotation(annotation)
                print(point)
            }
        }
        self.mapView.showAnnotations(self.mapView.annotations, animated: true)
        print("bye darkness")

    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func previousView(_ sender: Any) {
        self.dismiss(animated: false)
    }
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        
        let center = CLLocationCoordinate2DMake((location?.coordinate.latitude)!, (location?.coordinate.longitude)!)
        
        let region = MKCoordinateRegionMake(center, MKCoordinateSpan(latitudeDelta: 1,longitudeDelta: 1))
        
        self.mapView.setRegion(region, animated: true)
        self.locationManager.stopUpdatingLocation()
    }
    
    
    
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        var annotation: MKPointAnnotation = view.annotation! as! MKPointAnnotation
        if (annotation is MKPointAnnotation) {
            print("Clicked Pizza Shop\(annotation.accessibilityValue)")
            ref.child("messages").child(annotation.accessibilityValue!).observeSingleEvent(of: .value, with: { (snapshot) in
                // Get user value
                print(snapshot.value!)
                //let values = snapshot.value as! NSArray
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "chat2ID")
                self.present(vc!, animated: false, completion: nil)

            }) { (error) in
                print(error.localizedDescription)
            }
        }
    }
        //var alertView = UIAlertView(title: "Disclosure Pressed", message: "Click Cancel to Go Back", delegate: self, cancelButtonTitle: "Cancel", otherButtonTitles: "OK")
        //alertView.show()
    
    
    /*
    func mapView(_ mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView {
        // If it's the user location, just return nil.
        if (annotation is MKUserLocation) {
            print(annotation.title)
            return nil
        }
        // Handle any custom annotations.
        if (annotation is MKPointAnnotation) {
            // Try to dequeue an existing pin view first.

                // If an existing pin view was not available, create one.
                var pinView = MKAnnotationView(annotation: annotation, reuseIdentifier: "CustomPinAnnotationView")
                pinView.canShowCallout = true
                pinView.image = UIImage(named: "pizza_slice_32.png")
                pinView.calloutOffset = CGPoint(x: CGFloat(0), y: CGFloat(32))
                var rightButton = UIButton(type: .detailDisclosure)
                pinView.canShowCallout = true
                pinView.rightCalloutAccessoryView = rightButton
                // Add an image to the left callout.
                var iconView = UIImageView(image: UIImage(named: "pizza_slice_32.png"))
                pinView.leftCalloutAccessoryView = iconView

            return pinView
        }
        return nil
    }
 */
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
           /*
            let pin = mapView.view(for: annotation) as? MKPinAnnotationView ?? MKPinAnnotationView(annotation: annotation, reuseIdentifier: nil)
            pin.pinTintColor = .red
            return pin
            */
            return nil
        } else {
            /*
            // handle other annotations
            var pinView = MKAnnotationView(annotation: annotation, reuseIdentifier: "CustomPinAnnotationView")
            pinView.canShowCallout = true
            //pinView.image = UIImage(named: "pizza_slice_32.png")
            pinView.calloutOffset = CGPoint(x: CGFloat(0), y: CGFloat(32))
            var rightButton = UIButton(type: .detailDisclosure)
            pinView.canShowCallout = true
            pinView.rightCalloutAccessoryView = rightButton
            // Add an image to the left callout.
            var iconView = UIImageView(image: UIImage(named: "pizza_slice_32.png"))
            pinView.leftCalloutAccessoryView = iconView
            
            return pinView
 */
            let pin = mapView.view(for: annotation) as? MKPinAnnotationView ?? MKPinAnnotationView(annotation: annotation, reuseIdentifier: nil)
            pin.pinTintColor = .red
            var rightButton = UIButton(type: .detailDisclosure)
            pin.canShowCallout = true
            pin.rightCalloutAccessoryView = rightButton
            var iconView = UIImageView(image: UIImage(named: "pizza_slice_32.png"))
            pin.leftCalloutAccessoryView = iconView
            return pin

        }
        return nil
    }
    

    @IBAction func logout(_ sender: Any) {
        try! FIRAuth.auth()!.signOut()
        let loginManager = FBSDKLoginManager()
        loginManager.logOut()
            
        self.dismiss(animated: true, completion: nil)

    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
