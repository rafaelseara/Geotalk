//
//  File.swift
//  TheClotheadvisor
//
//  Created by Rafael Seara on 07/02/17.
//  Copyright © 2017 Rafael Seara. All rights reserved.
//

import Foundation


class mapTalkObj: NSObject {
    
    var id: String
    var lat: Double
    var long: Double
    var title: String
    var desc: String
    
    init(id: String, lat: Double, long: Double, title: String, desc: String) {
        self.id = id
        self.lat = lat
        self.long = long
        self.title = title
        self.desc = desc

    }
}
