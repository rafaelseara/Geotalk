//
//  MainMapViewController.swift
//  Geotalk
//
//  Created by Rafael Seara on 12/04/17.
//  Copyright © 2017 Rafael Seara. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import FBSDKLoginKit


class MainMapViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    
    private lazy var chatsRef: FIRDatabaseReference = FIRDatabase.database().reference().child("chats")
    //private lazy var messagesRef: FIRDatabaseReference = FIRDatabase.database().reference().child("messages")

    private var chatsRefHandle: FIRDatabaseHandle?

    let locationManager = CLLocationManager()
    
    var talkPoints = [mapTalkObj]()


    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.delegate = self
        
        // My locarion
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        
        self.locationManager.startUpdatingLocation()
        
        
        self.mapView.showsUserLocation = true
        //self.MapView.isZoomEnabled = false;
        //self.MapView.isScrollEnabled = false;

        
        print("didload in")
        //if (FIRAuth.auth().)
        observeChats()
        // Do any additional setup after loading the view.
        print("didload out")

    }
    
    override func viewWillAppear(_ animated: Bool) {
        //observeChats()
        self.showPoints()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // MARK: Firebase related methods
    private func observeChats() {
        // Use the observe method to listen for new
        // channels being written to the Firebase DB
        chatsRefHandle = chatsRef.observe(.childAdded, with: { (snapshot) -> Void in // 1
            let chatData = snapshot.value as! NSDictionary
            
            print(chatData)
            //let id =  chatData["id"] as! String
            let id = snapshot.key
            let lat = chatData["lat"] as! Double
            let lon = chatData["lon"] as! Double
            let title = chatData["subject"] as! String
            let desc = chatData["description"] as! String
            print("added")
            self.talkPoints.append(mapTalkObj.init(id: id, lat: lat, long: lon, title: title, desc: desc))
            self.showPoints()
        })
    }
    
    
    func showPoints(){
        print("Hello darkness")
        print(talkPoints.capacity)
        for point in talkPoints{
            
            let annotationView = MKAnnotationView()
            let detailButton: UIButton = UIButton.init(type: UIButtonType.detailDisclosure) as UIButton
            annotationView.rightCalloutAccessoryView = detailButton
            
            
            let myCoordinate = locationManager.location?.coordinate
            let location = CLLocationCoordinate2D(latitude: point.lat, longitude: point.long)
            let point1 = MKMapPointForCoordinate(myCoordinate!)
            let point2 = MKMapPointForCoordinate(location)
            
            let distanceInMeters = MKMetersBetweenMapPoints(point1, point2)
            
            //if(distanceInMeters <= 500){
                let annotation = MKPointAnnotation()
                annotationView.annotation = annotation
                annotation.coordinate = location
                annotation.title = point.title
                annotation.subtitle = point.desc
                annotation.accessibilityValue = point.id as String
                print(point.id)
                //let btn = UIButton(type: .detailDisclosure)
                //annotation.rightCalloutAccessoryView = btn
                mapView.addAnnotation(annotation)
                print(point)
            //}
        }
        self.mapView.showAnnotations(self.mapView.annotations, animated: true)
        print("bye darkness")
        
    }
    
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        
        let center = CLLocationCoordinate2DMake((location?.coordinate.latitude)!, (location?.coordinate.longitude)!)
        
        let region = MKCoordinateRegionMake(center, MKCoordinateSpan(latitudeDelta: 1,longitudeDelta: 1))
        
        self.mapView.setRegion(region, animated: true)
        self.locationManager.stopUpdatingLocation()
    }
    
    
    
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        var annotation: MKPointAnnotation = view.annotation! as! MKPointAnnotation
        
        if (annotation is MKPointAnnotation) {
            print("Clicked Pizza Shop\(annotation.accessibilityValue)")
            print("1")
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "chat2ID")
            print("2")

            vc?.accessibilityValue = annotation.accessibilityValue
            print("3")

            self.present(vc!, animated: true, completion: nil)
            print("4")


            /*
            messagesRef.child(annotation.accessibilityValue!).observeSingleEvent(of: .value, with: { (snapshot) in
                // Get user value
                print(snapshot.value!)
                //let values = snapshot.value as! NSArray
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "chat2ID")
                self.present(vc!, animated: false, completion: nil)
                
            }) { (error) in
                print(error.localizedDescription)
            }
             */
        }
        
    }

    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        } else {

            let pin = mapView.view(for: annotation) as? MKPinAnnotationView ?? MKPinAnnotationView(annotation: annotation, reuseIdentifier: nil)
            pin.pinTintColor = .red
            let rightButton = UIButton(type: .detailDisclosure)
            pin.canShowCallout = true
            pin.rightCalloutAccessoryView = rightButton
            let iconView = UIImageView(image: UIImage(named: "pizza_slice_32.png"))
            pin.leftCalloutAccessoryView = iconView
            return pin
            
        }
        return nil
    }

    
    
    
    @IBAction func logout(_ sender: Any) {
        try! FIRAuth.auth()!.signOut()
            let loginManager = FBSDKLoginManager()
            loginManager.logOut()
            
            self.dismiss(animated: true, completion: nil)
            
    }
    
    
    
    
}
