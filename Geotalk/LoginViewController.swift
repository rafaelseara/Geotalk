//
//  ViewController.swift
//  Geotalk
//
//  Created by Rafael Seara on 25/03/17.
//  Copyright © 2017 Rafael Seara. All rights reserved.
//

import UIKit
import Firebase
import FBSDKLoginKit


class LoginViewController: UIViewController, FBSDKLoginButtonDelegate {

    @IBOutlet weak var loginWith: UILabel!
    
    @IBOutlet weak var icon: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        icon.layer.cornerRadius = 20.0; // radius depend on how much rounded corner you want.
        icon.clipsToBounds = true;
        
        let loginButton = FBSDKLoginButton()
        loginButton.frame.size = .init(width: loginButton.frame.size.width * 1.7, height: loginButton.frame.size.height * 1.7)
        
        loginButton.center = CGPoint(x:self.view.center.x
            , y: self.view.center.y + 200
)
        loginButton.readPermissions = ["public_profile", "email", "user_friends"]
        loginButton.delegate = self
        self.view.addSubview(loginButton)
    }

    override func viewDidAppear(_ animated: Bool) {
        if ((FBSDKAccessToken.current()) != nil) {
            print("here")
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "mapID")
            self.present(vc!, animated: true, completion: nil)
            
        }
    }
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error?) {
        
        print("hello1")
        
        if let error = error {
            print(error.localizedDescription)
            return
        }
        // ...
        
        let credential = FIRFacebookAuthProvider.credential(withAccessToken: FBSDKAccessToken.current().tokenString)

        FIRAuth.auth()?.signIn(with: credential) { (user, error) in
            if error == nil {
                print("You have successfully signed up")
                //Goes to the Setup page which lets the user take a photo for their profile picture and also chose a username
                let vc = self.storyboard?.instantiateViewController(withIdentifier: "mapID")
                self.present(vc!, animated: true, completion: nil)
        
                
                //let vc = self.storyboard?.instantiateViewController(withIdentifier: "registerID")
                //self.present(vc!, animated: true, completion: nil)
                
                
            } else {
                let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                
                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alertController.addAction(defaultAction)
                
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    
    @IBAction func guestLogin(_ sender: Any) {
        FIRAuth.auth()?.signInAnonymously(completion: { (user, error) in // 2
            if let err = error { // 3
                print(err.localizedDescription)
                return
            }
            let vc = self.storyboard?.instantiateViewController(withIdentifier: "mapID")
            self.present(vc!, animated: true, completion: nil)
        })
    }
    
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("User Logged Out")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

