//
//  NewTalkViewController.swift
//  Geotalk
//
//  Created by Rafael Seara on 26/03/17.
//  Copyright © 2017 Rafael Seara. All rights reserved.
//

import UIKit
import CoreLocation


class NewTalkViewController: UIViewController,CLLocationManagerDelegate {

    let locationManager = CLLocationManager()

    
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var desc: UITextField!
    
    var talkPoints = [mapTalkObj]()

    //let ref = FIRDatabase.database().reference();

    private lazy var chatsRef: FIRDatabaseReference = FIRDatabase.database().reference().child("chats")

    
    @IBAction func doneTalk(_ sender: Any) {
        
        let myCoordinate = locationManager.location?.coordinate

        
        if let subject = self.name?.text { // 1
            if let descript = self.desc?.text {
                let newChannelRef = chatsRef.childByAutoId()
                let chatItem = [
                    "id": UUID().uuidString,
                    "lat": myCoordinate?.latitude,
                    "lon": myCoordinate?.longitude,
                    "subject": subject,
                    "description": descript
                ] as NSDictionary
                
                newChannelRef.setValue(chatItem) // 4
                
            }
        }
        
        self.dismiss(animated: true, completion: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        name.becomeFirstResponder()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func cancelTalk(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }

    
    /*
        // MARK :Actions
        @IBAction func createChannel(_ sender: AnyObject) {
            if let name = newChannelTextField?.text { // 1
                let newChannelRef = channelRef.childByAutoId() // 2
                let channelItem = [ // 3
                    "name": name
                ]
            newChannelRef.setValue(channelItem) // 4
            }
        }
 */

}
