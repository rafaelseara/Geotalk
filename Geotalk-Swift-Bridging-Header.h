//
//  Geotalk-Swift-Bridging-Header.h
//  Geotalk
//
//  Created by Rafael Seara on 25/03/17.
//  Copyright © 2017 Rafael Seara. All rights reserved.
//

#ifndef Geotalk_Swift_Bridging_Header_h
#define Geotalk_Swift_Bridging_Header_h

#import <Firebase/Firebase.h>
#import "JSQMessagesViewController/JSQMessages.h"
#import "JSQMessagesViewController/JSQMessageData.h"
#import "JSQMessagesViewController/JSQMessagesAvatarImageFactory.h"


#endif /* Geotalk_Swift_Bridging_Header_h */
